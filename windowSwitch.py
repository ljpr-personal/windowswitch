import os
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
import subprocess

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
cmd = subprocess.Popen(["wmctrl -l|awk '{$3=None; $1=None; print $0}'"], shell=True, stdout=subprocess.PIPE).stdout
windowList = cmd.read().decode().splitlines()
windowListSpre = sorted(windowList)
windowListS = [i for i in windowListSpre if not ('Top Panel' in i or 'plank' in i)]
buttonList = []
for i in range (0, len(windowListS)):
    buttonName = "button" + str(i)
    buttonList.append(buttonName)

class ButtonWindow(Gtk.Window):

    def __init__(self, _buttonList, _windowList):
        Gtk.Window.__init__(self, title="app Switcher!")
        self.set_border_width(10)
        vbox = Gtk.VBox(spacing=4)
        self.add(vbox)

        #self.set_border_width(6)

        for i in range (0, len(_windowList)):
            _buttonList[i] = Gtk.Button.new_with_label(_windowList[i])
            _buttonList[i].connect("clicked", self.on_button_clicked)
            vbox.pack_start(_buttonList[i], True, True, 0)

    def on_button_clicked(self, _button):
        buttonLabelStr = _button.get_label()
        print(buttonLabelStr)
        _window = buttonLabelStr[4:]
        cmd = "wmctrl -a '" + _window + "'"
        os.system(cmd)
        self.close()

win = ButtonWindow(buttonList, windowListS)
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()